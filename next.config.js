// next.config.js
module.exports = {
  trailingSlash: true,
  reactStrictMode: true,
  i18n: {
    locales: ['en', 'fr'],
    defaultLocale: 'fr',
    localeDetection: false,
  },
};
